

public class Test {
    //苹果
    int apple_price = 8;
    //草莓
    int strawberry_price = 13;
    //芒果
    int mango_price = 20;

    public int calculate_total_price(int apple_weight, int strawberry_weight) {
        return apple_weight * apple_price + strawberry_weight * strawberry_price;
    }
    public int calculate_total_price2(int apple_weight, int strawberry_weight, int mango_weight) {
        return apple_weight * apple_price + strawberry_weight * strawberry_price + mango_weight * mango_price;
    }
    public double calculate_total_price3(int apple_weight, int strawberry_weight, int mango_weight) {
        double new_strawberry_price = strawberry_price * 0.8;
        return apple_weight * apple_price + strawberry_weight *new_strawberry_price  + mango_weight * mango_price;
    }

    public double calculate_total_price4(int apple_weight, int strawberry_weight, int mango_weight) {
        double new_strawberry_price = strawberry_price * 0.8;
        double money= apple_weight * apple_price + strawberry_weight *new_strawberry_price  + mango_weight * mango_price;
        return  money>=100?money-10:money;
    }

    public static void main(String[] args) {
        Test test = new Test();
        int calculateTotalPrice = test.calculate_total_price(5, 6);
        System.out.println("第一题总价" + calculateTotalPrice);

        int calculateTotalPrice2 = test.calculate_total_price2(2, 3, 2);
        System.out.println("第二题总价" + calculateTotalPrice2);

        double calculate_total_price3 = test.calculate_total_price3(2, 3, 2);
        System.out.println("第三题总价" + calculate_total_price3);

        double calculate_total_price4 = test.calculate_total_price4(2, 3, 6);
        System.out.println("第四题总价" + calculate_total_price4);

    }
}


;